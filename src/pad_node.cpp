#include "ros/ros.h"
#include "sensor_msgs/Joy.h"
#include "geometry_msgs/Twist.h"
#include "robot_msgs/lights.h"
#include "pad/XBOXController.h"


geometry_msgs::Twist vel;
double max_linear;
double max_angular;
double dead_zone;

ros::Subscriber sub;
ros::Publisher pub_vel;
ros::Publisher pub_lights;


void joy_call_back (const sensor_msgs::Joy::ConstPtr& msg) {
    
        vel.linear.x = 0;
        vel.angular.z =0;
    
    if (msg->buttons[(int)(XBOXButtons::LB)] || msg->buttons[(int)(XBOXButtons::RB)]) {

        if (fabs(msg->axes[(int)(XBOXAxis::RSV)])>dead_zone) {
          
        vel.linear.x = (msg->axes[(int)(XBOXAxis::RSV)])*max_linear;

        }
         if (fabs(msg->axes[(int)(XBOXAxis::RSH)])>dead_zone) {
            
           vel.angular.z = (msg->axes[(int)(XBOXAxis::RSH)])*max_angular;
         }
    }
    
      robot_msgs::lights lights;
      
      if (msg->buttons[(int)(XBOXButtons::Y)]) {
          lights.lights_front_lock = true;
          
      }
      
      if (msg->buttons[(int)(XBOXButtons::B)]) {
          lights.lights_error_lock = true;
      }
      
      if (msg->buttons[(int)(XBOXButtons::X)]) {
          lights.lights_auto_lock = true;
      }
      
      if (msg->buttons[(int)(XBOXButtons::A)]) {
          lights.lights_cruise_lock = true;
      }
      
      if (msg->buttons[(int)(XBOXButtons::BACK)]) {
          lights.lights_end_lock = true;
      }
      pub_lights.publish(lights);
        
        
     
    
    
}





void publishvel () {
    pub_vel.publish(vel);
}



int main(int argc, char** argv) {
    
    ros::init (argc, argv, "pad");

    ros::NodeHandle np;
    
    if(np.getParam("linear_vel_max", max_linear)){
        std::cout<<"udalo sie"<<std::endl;
        
    }
    else {
        std::cout<<"nie udalo sie"<<std::endl;
    }

    np.param<double>("linear_vel_max", max_linear, 0.5);
    np.param<double>("angular_vel_max", max_angular, 0.5);
    np.param<double>("dead_zone_max", dead_zone, 0.1);
    
    sub = np.subscribe("joy", 1, joy_call_back);
   
    pub_vel = np.advertise<geometry_msgs::Twist>("robot/cmd_vel", 1);
    
    pub_lights = np.advertise<robot_msgs::lights>("robot/lights", 1);
    
    ros::Rate rate(20);
    
    while (ros::ok()) {
        
        ros::spinOnce();
        publishvel();
        rate.sleep();
        
        
    }
    

    return 0;
}

