/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   XBOXController.h
 * Author: jannoszczyk
 *
 * Created on August 23, 2016, 10:18 AM
 */

#ifndef XBOXCONTROLLER_H
#define XBOXCONTROLLER_H

enum class XBOXButtons {
    
    //guziczki
    A=0,
    B=1,
    X=2,
    Y=3,
    
    //tylne
    LB=4,
    RB=5,
    
    BACK=6,
    START=7,
    MAIN=8,
    
    //galki
    LS=9,
    RS=10,
    
    //strzalki
    L=11,
    R=12,
    U=13,
    D=14
    
};



enum class XBOXAxis {
    
    LSH=0,
    LSV=1,
    
    LT=2, //
    
    RSH=3,
    RSV=4,
    
    RT=5 ///
    
    
};










#endif /* XBOXCONTROLLER_H */

